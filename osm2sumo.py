#!/usr/bin/env python3

# osm2sumo is a tool to convert OpenStreet Map (OSM) data to maps that are usable in
# OMNET++/SUMO simulations using the Veins extension.
#
# Copyright 2017 OnBoard Security, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Author: Mohammad Raashid Ansari
#
# Usage of this tool in any research or tool or any sort of future work that uses this tool
# requires a recognition to OnBoard Security, Inc. in the form of textual, audio or visual
# acknowlegement.

import sys
import math
import os
import json
import argparse
import subprocess
import xml.etree.ElementTree as et


# define global variables
CWD = os.getcwd()
NET_FILE_EXT = '.net.xml'
ROUTE_FILE_EXT = '.rou.xml'
OSM_FILE_EXT = '_bbox.osm.xml'
POLY_FILE_EXT = '.poly.xml'
SUMO_CONF_EXT = '.sumo.cfg'
OMNET_LAUNCH_CONFIG_EXT = '.launchd.xml'

# define command line parser
def parseArgs():
    parser = argparse.ArgumentParser(prog='osm2sumo', description='''%(prog)s
            extracts a portion of the desired map from openstreetmaps.org and
            converts it to a set of files understood my SUMO (traffic simulator)
            and OMNET++ (network simulator). It also creates a set of files useful
            to set up a simulation in OMNET++''')
    parser.add_argument('latitude', type=float, help='latitude in decimal')
    parser.add_argument('longitude', type=float, help='longitude in decimal')
    parser.add_argument('radius', type=float, help='radius of the map to download')
    parser.add_argument('cityName', help='name of the map')
    parser.add_argument('-g', "--run-osm-get", action="store_true",
            help='get osm from openstreet')
    parser.add_argument('-b', '--run-osm-build', action="store_true",
            help='build from osm file')
    parser.add_argument('-d', '--create-veh-dist', action="store_true",
                    help='generate vehicle distributions')
    parser.add_argument('-t', '--create-trips', action="store_true",
            help='generate trips on osm')
    parser.add_argument('-s', '--create-sumo-config', action="store_true",
            help='create configuration file for SUMO')
    parser.add_argument('-r', '--run-sumo', action="store_true",
            help='run SUMO with configuration')
    parser.add_argument('-l', "--create-omnet-launchd", action="store_true",
            help='create OMNET-SUMO launchd configuration')
    parser.add_argument('-m', '--create-omnet-config', action="store_true",
            help='create configuration file for OMNET++')
    parser.add_argument('--start-sumo-launcher', action='store_true',
            help='start sumo-launchd.py script')
    parser.add_argument('-a', '--all', action='store_true',
            help='run all commands without the need to give respective options')
    parser.add_argument('-v', '--verbose', action='store_true',
            help='show verbose output of script run')
    return parser.parse_args()


def prettify_xml(root_element):
    '''print xml in human-readable format'''
    import xml.dom.minidom as md
    rough_string = et.tostring(root_element, 'UTF-8')
    reparsed = md.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ", encoding='UTF-8').decode(encoding="UTF-8")


class Config:
    def __init__(self, logger, args):
        self.configDir = "config"
        self.outputDir = "cities"
        self.configFile = "osm2sumo.json"
        self.coordsDir = "coordinates"
        self.sumoCfg = "{0}/{1}/sumoCfg/{1}".format(self.outputDir, args.cityName)
        self.extConfig = {}

        self.logger = logger
        self.logger.log("{}.{}".format(self.__class__.__name__, self.__init__.__name__))

    def _createNewConfig(self):
        self.logger.log("{}.{}".format(self.__class__.__name__, self._createNewConfig.__name__))
        self.extConfig["typemapDir"] = input("Enter path to typemap directory: ")
        self.extConfig["sumoToolsDir"] = input("Enter path to the SUMO tools directory: ")
        self.extConfig["veinsDir"] = input("Enter path to VEINS directory: ")

        configFileContent = json.JSONEncoder(indent=2).encode(self.extConfig)
        self.logger.log("config file content: {}".format(configFileContent))

        with open("{}/{}".format(self.configDir, self.configFile), 'w') as configFh:
            self.logger.log("Writing to file: {}".format(configFh))

            try:
                result = configFh.write(configFileContent)
                if result == 0:
                    raise IOError
            except IOError as ie:
                log_message = "Error writing config {}".format(ie)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                return -1
            else:
                log_message = "Wrote config: {}".format(configFileContent)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                return 0

    def loadConfig(self):
        self.logger.log("{}.{}".format(self.__class__.__name__, self.loadConfig.__name__))
        result = 0
        try:
            configFh = open("{}/{}".format(self.configDir,self.configFile), 'r')
            self.logger.log("Reading config file: {}".format(configFh))
        except FileNotFoundError as ferror:
            log_message = "no config file (osm2sumo.json) found."
            if args.verbose:
                print(log_message)
            self.logger.log(log_message)

            result = self._createNewConfig()
        else:
            try:
                self.extConfig = json.load(configFh)
                if self.extConfig == None:
                    raise IOError
            except IOError as ie:
                log_message = "Error loading osm2sumo config {}".format(ie)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                result = -1
            else:
                log_message = "Loaded config: {}".format(self.extConfig)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                configFh.close()
                result = 0

        env = os.environ.copy()
        env["PATH"] = "{}{}".format(env["PATH"], self.getConfig())
        self.sysPath = env
        return result


    def getConfig(self):
        return ":{}:{}:{}".format(self.extConfig["typemapDir"],
                self.extConfig["sumoToolsDir"], self.extConfig["veinsDir"])


class Log:
    def __init__(self, logFileName):
        self.logFile = logFileName
        try:
            self.fh = open(self.logFile, 'a')
        except FileNotFoundError as ferror:
            print(ferror)
 
    def log(self, logText):
        try:
            result = self.fh.write("{}\n".format(logText))
            if result == 0:
                raise IOError
        except IOError as ie:
            log_message = "Error logging text: {}, error: {}".format(logText, ie)
            print(log_message)
            sys.exit(-1)

    def close(self):
        self.fh.close()


class Osm2Sumo:
    def __init__(self, logger, config, args):
        self.cityName = args.cityName
        self.lon = args.longitude
        self.lat = args.latitude
        self.radius = args.radius
        self.logger = logger
        self.config = config
        self.sumoCfg = config.sumoCfg
        self.logger.log("PATH={}".format(config.sysPath["PATH"]))
        self.logger.log("{}.{}".format(self.__class__.__name__, self.__init__.__name__))

    def getCoordinates(self):
        '''calculate coordinates for the bbox to input to osmGet.py'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.getCoordinates.__name__))
        if self.radius == 0:
            print("Radius needs to be greater than 0")
            sys.exit(2)

        RADIUS_OF_EARTH = 6371e3
        lat_r1 = math.radians(float(self.lat))
        lon_r1 = math.radians(float(self.lon))
        angular_distance = (float(self.radius) * 1000) / RADIUS_OF_EARTH
        # hypotenuse = math.sqrt((float(radius) * 1000 * float(radius) * 1000) * 2)
        # angular_distance = hypotenuse / RADIUS_OF_EARTH
        # Get South-West, and North-East Coordinates
        # angles = [225, 45]
        # Get North, West, South, and East
        angles = [0, 90, 180, 270]
        lat_r2 = []
        lon_r2 = []
        for angle in angles:
            lat2 = math.asin(
                math.sin(lat_r1) * math.cos(angular_distance)
                + math.cos(lat_r1) * math.sin(angular_distance)
                * math.cos(angle)
                )
            lat_r2.append('{:.8}'.format(str(math.degrees(lat2))))
            lon2 = (lon_r1 + math.atan2(math.sin(angle) *
                    math.sin(angular_distance) * math.cos(lat_r1),
                    math.cos(angular_distance) - math.sin(lat_r1) *
                    math.sin(lat2)) + 540) % 360 - 180
            lon_r2.append('{:.8}'.format(str(math.degrees(lon2))))
        # Return a tuple with the box values for osmGet.py
        # Format: <LAT_MIN>, <LONG_MIN>, <LAT_MAX>, <LONG_MAX>
        # South, West, North, East
        self.south = lat_r2[2]
        self.west = lon_r2[3]
        self.north = lat_r2[0]
        self.east = lon_r2[1]
        self.logger.log("Got coordinates: (East, West, North, South) = ({}, {}, {}, {})".format(self.east, self.west, self.north, self.south))


    def getOsmMap(self):
        '''
        download map from openstreepmap.org according to the calculated
        coordinates
        '''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.getOsmMap.__name__))
        log_message = "Getting map...."
        print(log_message)
        self.logger.log(log_message)

        command = ['osmGet.py']
        command.append('--bbox')
        command.append("{},{},{},{}".format(self.south, self.west, self.north, self.east))
        command.append('--prefix')
        command.append(self.sumoCfg)

        if args.verbose:
            print(command)
        self.logger.log(command.__repr__())
        self.logger.log("Environment variables: {}".format(self.config.sysPath["PATH"]))
        return self.run(command)


    def buildRoadNetwork(self):
        '''build .net.xml and .poly.xml files from map in .osm file'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.buildRoadNetwork.__name__))
        log_message = "Building network and routing files...."
        print(log_message)
        self.logger.log(log_message)

        command = ['osmBuild.py']
        command.append('--prefix')
        command.append(self.sumoCfg)
        command.append('--osm-file')
        command.append("{}{}".format(self.sumoCfg, OSM_FILE_EXT))
        command.append('--vehicle-classes')
        command.append('all')
        command.append('--typemap')
        command.append("{}/Polyconvert.typ.xml".format(config.extConfig["typemapDir"]))
        # command += ' --netconvert-options '
        # command += '-v,false'
        # command += '--tls.unset,42437644'
        command.append('--netconvert-typemap')
        command.append("{}/Netconvert.typ.xml".format(config.extConfig["typemapDir"]))
        command.append('--polyconvert-options')
        # command += '-v,false'
        command.append('--osm.keep-full-type,false')
        # command += ', --osm-files, ' + file_name + OSM_FILE_EXT
        # command += ','

        if args.verbose:
            print(command)
        self.logger.log(command.__repr__())
        return self.run(command)


    def addAddtionalVehTypes(self):
        '''add new vehicle types'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.createTrips.__name__))
        log_message = "Creating additional file for vTypes..."
        print(log_message)
        self.logger.log(log_message)

        additional_tag = et.Element("additional")
        new_vehicle_type = et.SubElement(additional_tag, 'vType', id="bait",
                accel="2.6", decel="4.5", sigma="0.5", length="2.5",
                minGap="2.5", maxSpeed="14", color="1,0,1", vClass="passenger")
        new_vehicle_type = et.SubElement(additional_tag, 'vType', id="victim",
                accel="2.6", decel="4.5", sigma="0.5", length="2.5",
                minGap="2.5", maxSpeed="14", color="1,1,0", vClass="passenger")
        new_vehicle_type = et.SubElement(additional_tag, 'vType',
                id="attacker", accel="2.6", decel="4.5", sigma="0.5",
                length="2.5", minGap="2.5", maxSpeed="14", color="1,0,0",
                vClass="passenger")
        tree = et.ElementTree(additional_tag)
        self.vehTypeAddFile = "{}.vType.xml".format(self.sumoCfg)
        tree.write(self.vehTypeAddFile)


    def createTrips(self):
        '''
        generate random trips for 100 vehicles using randomTrips.py
        '''
        log_message = "Creating trips..."
        print(log_message)
        self.logger.log(log_message)

        command = ['randomTrips.py']
        command.append('-n') # net-file
        command.append("{}{}".format(self.sumoCfg, NET_FILE_EXT))
        command.append('-r') # output route-file
        command.append("{}{}".format(self.sumoCfg, ROUTE_FILE_EXT))
        command.append('--end')
        command.append('100')
        command.append('-l')
        command.append('-a') # additional-files
        command.append(self.vehTypeAddFile)

        if args.verbose:
            print(command)
        self.logger.log(command.__repr__())
        return self.run(command)


    def createVehTypeDist(self):
        '''generate distribution for vTypes'''
        command = ['createVehTypeDistribution.py']
        command.append('{}/carTypeDist.txt'.format(self.config.configDir))
        command.append('--output-file')
        command.append("{}.additional{}".format(self.sumoCfg, ROUTE_FILE_EXT))
        command.append('--name')
        command.append('genuine')

        if args.verbose:
            print(command)
        self.logger.log(command.__repr__())
        # self.run(command)

        # command = ['createVehTypeDistribution.py']
        # command.append('attackCarTypeDist.txt')
        # command.append('--output-file')
        # command.append("{}{}".format(self.sumoCfg, ROUTE_FILE_EXT))

        # if args.verbose:
            # print(command)
        # self.logger.log(command.__repr__())
        return self.run(command)


    def createSumoConfig(self):
        '''generate a configuration file for SUMO'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.createSumoConfig.__name__))
        log_message = 'Writing SUMO config file...'
        print(log_message)
        self.logger.log(log_message)

        command = ['sumo']
        command.append('--save-configuration')
        command.append("{}{}".format(self.sumoCfg, SUMO_CONF_EXT))
        command.append('--net-file')
        command.append("{}{}".format(self.cityName, NET_FILE_EXT))
        command.append('--route-files')
        command.append("{}{}".format(self.cityName, ROUTE_FILE_EXT))
        command.append('--additional-files')
        command.append("{}{}".format(self.cityName, POLY_FILE_EXT))
        # command.append('--output-prefix ' + file_name + ' '
        command.append('--begin')
        command.append('0')
        command.append('--end')
        command.append('1000')
        command.append('--step-length')
        command.append('0.1')
        command.append('--start')
        command.append('true')

        if args.verbose:
            print(command)
        self.logger.log(command.__repr__())
        return self.run(command)


    def runSumo(self):
        '''launch SUMO with the provided configuration file'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.runSumo.__name__))
        log_message = 'Running Sumo simulation'
        print(log_message)
        self.logger.log(log_message)

        command = ['sumo-gui']
        command.append('-c')
        command.append("{}/{}{}".format(CWD, self.sumoCfg, SUMO_CONF_EXT))

        if args.verbose:
            print(command)
        self.logger.log(command.__repr__())
        return self.run(command)


    def createOmnetLaunchd(self):
        '''generate the configuration file for sumo-launchd.py'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.createOmnetLaunchd.__name__))
        log_message = 'Creating OMNET launchd.xml...'
        print(log_message)
        self.logger.log(log_message)

        launch_tag = et.Element('launch')
        launch_tag.append(et.Comment('debug config'))
        copy_tag = et.SubElement(launch_tag, 'copy',
                file="{}{}".format(self.cityName, NET_FILE_EXT))
        copy_tag = et.SubElement(launch_tag, 'copy',
                file="{}{}".format(self.cityName, ROUTE_FILE_EXT))
        copy_tag = et.SubElement(launch_tag, 'copy',
                file="{}{}".format(self.cityName, POLY_FILE_EXT))
        copy_tag = et.SubElement(launch_tag, 'copy',
                file="{}{}".format(self.cityName, SUMO_CONF_EXT), type='config')

        with open("{}{}".format(self.sumoCfg, OMNET_LAUNCH_CONFIG_EXT), 'w') as file_handle:
            launch_config = prettify_xml(launch_tag)
            try:
                result = file_handle.write(launch_config)
                if result == 0:
                    raise IOError
            except IOError as ie:
                log_message = "Error writing omnet++ launch config {}".format(ie)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                return -1
            else:
                log_message = "Wrote omnet launchd config: {}".format(launch_config)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                return 0


        # tree = et.ElementTree(launch_tag)
        # tree.write(file_name + OMNET_LAUNCH_CONFIG_EXT, xml_declaration = True, encoding = 'utf-8', method = 'xml')


    def createOmnetConfig(self):
        '''generate the configuration file for OMNET++'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.createOmnetConfig.__name__))
        log_message = 'Creating OMNET Config...'
        print(log_message)
        self.logger.log(log_message)

        root = et.Element('root')
        analog_models = et.SubElement(root, 'AnalogueModels')
        analog_model = et.SubElement(analog_models, 'AnalogueModel', type='SimplePathlossModel')
        parameter = et.SubElement(analog_model, 'parameter', name='alpha', type='double', value = '2.0')
        parameter = et.SubElement(analog_model, 'parameter', name='carrierFrequency', type='double', value='5.890e+9')
        analog_model = et.SubElement(analog_models, 'AnalogueModel', type='SimpleObstacleShadowing')
        parameter = et.SubElement(analog_model, 'parameter', name='carrierFrequency', type='double', value='5.890e+9')
        obstacles = et.SubElement(analog_model, 'obstacles')
        type_tag = et.SubElement(obstacles, 'type', attrib={'id':'building', 'db-per-cut':'9', 'db-per-meter':'0.4'})
        decider = et.SubElement(root, 'Decider', type='Decider80211p')
        decider.append(et.Comment('The center frequency on which the phy listens'))
        parameter = et.SubElement(decider, 'parameter', name='centerFrequency', type='double', value='5.890e9')

        with open("{}/{}/config.xml".format(self.config.outputDir, self.cityName), 'w') as file_handle:
            omnet_config = prettify_xml(root)
            try:
                result = file_handle.write(omnet_config)
                if result == 0:
                    raise IOError
            except IOError as ie:
                log_message = "Error writing omnet++ config.xml {}".format(ie)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                return -1
            else:
                log_message = "Wrote omnet config.xml: {}".format(omnet_config)
                if args.verbose:
                    print(log_message)
                self.logger.log(log_message)
                return 0


    def startSumoLauncher(self):
        '''launch sumo-launchd.py with default options'''
        self.logger.log("{}.{}".format(self.__class__.__name__, self.startSumoLauncher.__name__))
        log_message = "Starting SUMO daemon launcher..."
        print(log_message)
        self.logger.log(log_message)

        command = ['sumo-launchd.py']
        command.append('-vv')
        command.append('-c')
        command.append('sumo')

        if args.verbose:
            print(command)
        self.logger.log(command.__repr__())
        return self.run(command)

    def run(self, command):
        self.logger.log("Running command: {}".format(command.__repr__()))
        result = subprocess.Popen(command, env=self.config.sysPath).wait()
        if not result == 0:
            log_message = "error running command, returned result {}".format(result)
            if args.verbose:
                print(log_message)
            self.logger.log(log_message)
        return result


# TODO: Find a way to import map images through command line
# Current options are: MapQuest API, MapBox API (Limited free usage)
# Manual option is through the "Export" button on openstreetmap.org
# The manual step could be automated using Selenium


if __name__ == '__main__':
    args = parseArgs()
    logger = Log("osm2sumo.log")
    config = Config(logger, args)

    if not os.path.isdir(config.configDir):
        os.mkdir("{}".format(config.configDir))

    if not os.path.isdir(config.outputDir):
        os.mkdir("{}".format(config.outputDir))

    if not os.path.isdir(config.coordsDir):
        os.mkdir("{}".format(config.coordsDir))

    if not os.path.isdir("{}/{}".format(config.outputDir,args.cityName)):
        os.mkdir("{}/{}".format(config.outputDir, args.cityName))
        os.mkdir("{}/{}/sumoCfg".format(config.outputDir, args.cityName))

    config.loadConfig()

    o2s = Osm2Sumo(logger, config, args)
    o2s.getCoordinates()

    Log('{}/{}_coords.txt'.format(config.coordsDir, args.cityName)).log(
            "{} {}".format(args.latitude, args.longitude))

    if args.run_osm_get or args.all:
        result = o2s.getOsmMap()
        if not result == 0:
            sys.exit(result)

    if args.run_osm_build or args.all:
        result = o2s.buildRoadNetwork()
        if not result == 0:
            sys.exit(result)

    if args.create_trips or args.all:
        o2s.addAddtionalVehTypes()
        result = o2s.createTrips()
        if not result == 0:
            sys.exit(result)

    if args.create_veh_dist or args.all:
        result = o2s.createVehTypeDist()
        if not result == 0:
            sys.exit(result)

    if args.create_sumo_config or args.all:
        result = o2s.createSumoConfig()
        if not result == 0:
            sys.exit(result)

    if args.create_omnet_launchd or args.all:
        result = o2s.createOmnetLaunchd()
        if not result == 0:
            sys.exit(result)

    if args.create_omnet_config or args.all:
        result = o2s.createOmnetConfig()
        if not result == 0:
            sys.exit(result)

    if args.start_sumo_launcher or args.all:
        result = o2s.startSumoLauncher()
        if not result == 0:
            sys.exit(result)

    if args.run_sumo or args.all:
        result = o2s.runSumo()
        if not result == 0:
            sys.exit(result)

    logger.close()

