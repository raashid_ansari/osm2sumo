# OSM to SUMO/OMNET++/Veins map converter
osm2sumo extracts a portion of the desired map from openstreetmaps.org and
converts it to a set of files understood my SUMO (traffic simulator) and
OMNET++ (network simulator). It also creates a set of files useful to set up a
simulation in OMNET++

This tool uses command line tools developed by the developers of SUMO traffic simulator.

### Usage:
```
osm2sumo [-h] [-g] [-b] [-t] [-s] [-r] [-l] [-o]
                [--start-sumo-launcher] [-a] [-v]
                latitude longitude radius cityName

positional arguments:
  latitude              latitude in decimal
  longitude             longitude in decimal
  radius                radius of the map to download
  cityName              name of the map

optional arguments:
  -h, --help            show this help message and exit
  -g, --run-osm-get     get osm from openstreet
  -b, --run-osm-build   build from osm file
  -t, --create-trips    generate trips on osm
  -s, --create-sumo-config
                        create configuration file for SUMO
  -r, --run-sumo        run SUMO with configuration
  -l, --create-omnet-launchd
                        create OMNET-SUMO launchd configuration
  -o, --create-omnet-config
                        create configuration file for OMNET++
  --start-sumo-launcher
                        start sumo-launchd.py script
  -a, --all             run all commands without the need to give respective
                        options
  -v, --verbose         show verbose output of script run
```